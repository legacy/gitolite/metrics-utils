/* Copyright 2009 The Tor Project
 * See LICENSE for licensing information */

import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import org.bouncycastle.util.encoders.Base64;

public final class ExoneraTor {

  public static void main(final String[] args) throws Exception {

    // check parameters
    if (args.length < 4 || args.length > 5) {
      System.err.println("\nUsage: java "
          + ExoneraTor.class.getSimpleName()
          + " <descriptor archive directory> <IP address in question> "
          + "<timestamp, in UTC, formatted as YYYY-MM-DD hh:mm:ss> "
          + "[<target address>[:<target port>]]\n");
      return;
    }
    File archiveDirectory = new File(args[0]);
    if (!archiveDirectory.exists() || !archiveDirectory.isDirectory()) {
      System.err.println("\nDescriptor archive directory + "
            + archiveDirectory.getAbsolutePath()
            + " does not exist or is not a directory.\n");
      return;
    }
    String relayIP = args[1];
    String timestampStr = args[2] + " " + args[3];
    SimpleDateFormat timeFormat = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss");
    timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    long timestamp = timeFormat.parse(timestampStr).getTime();
    String target = null, targetIP = null, targetPort = null;
    String[] targetIPParts = null;
    if (args.length > 4) {
      target = args[4];
      if (target.contains(":")) {
        targetIP = target.split(":")[0];
        targetPort = target.split(":")[1];
      } else {
        targetIP = target;
      }
      targetIPParts = targetIP.replace(".", " ").split(" ");
    }
    String DELIMITER = "--------------------------------------------------"
        + "-------------------------";
    System.out.println("\nTrying to find out whether " + relayIP + " was "
        + "running as a Tor relay at " + timestampStr
        + (target != null ? " permitting exiting to " + target : "")
        + "...\n\n" + DELIMITER);

    // check that we have the required archives
    long timestampTooOld = timestamp - 300 * 60 * 1000;
    long timestampFrom = timestamp - 180 * 60 * 1000;
    long timestampTooNew = timestamp + 120 * 60 * 1000;
    Calendar calTooOld = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    Calendar calFrom = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    Calendar calTooNew = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    calTooOld.setTimeInMillis(timestampTooOld);
    calFrom.setTimeInMillis(timestampFrom);
    calTooNew.setTimeInMillis(timestampTooNew);
    System.out.printf("%nChecking that relevant archives between "
        + "%tF %<tT and %tF %<tT are available...%n", calTooOld,
        calTooNew);
    SortedSet<String> requiredDirs = new TreeSet<String>();
    requiredDirs.add(String.format("consensuses-%tY-%<tm", calTooOld));
    requiredDirs.add(String.format("consensuses-%tY-%<tm", calTooNew));
    if (target != null) {
      requiredDirs.add(String.format("server-descriptors-%tY-%<tm",
          calTooOld));
      requiredDirs.add(String.format("server-descriptors-%tY-%<tm",
          calTooNew));
    }
    SortedSet<File> consensusDirs = new TreeSet<File>();
    SortedSet<File> descriptorsDirs = new TreeSet<File>();
    Stack<File> directoriesLeftToParse = new Stack<File>();
    directoriesLeftToParse.push(archiveDirectory);
    while (!directoriesLeftToParse.isEmpty()) {
      File directoryOrFile = directoriesLeftToParse.pop();
      if (directoryOrFile.getName().startsWith("consensuses-")) {
        if (requiredDirs.contains(directoryOrFile.getName())) {
          requiredDirs.remove(directoryOrFile.getName());
          consensusDirs.add(directoryOrFile);
        }
      } else if (directoryOrFile.getName().startsWith(
          "server-descriptors-")) {
        if (requiredDirs.contains(directoryOrFile.getName())) {
          requiredDirs.remove(directoryOrFile.getName());
          descriptorsDirs.add(directoryOrFile);
        }
      } else {
        for (File fileInDir : directoryOrFile.listFiles())
          if (fileInDir.isDirectory())
            directoriesLeftToParse.push(fileInDir);
      }
    }
    for (File dir : consensusDirs)
      System.out.println("  " + dir.getAbsolutePath());
    for (File dir : descriptorsDirs)
      System.out.println("  " + dir.getAbsolutePath());
    if (!requiredDirs.isEmpty()) {
      System.out.println("\nWe are missing consensuses and/or server "
          + "descriptors. Please download these archives and extract them "
          + "to your data directory. Be sure NOT to rename the extracted "
          + "directories or the contained files.");
      for (String dir : requiredDirs)
        System.out.println("  " + dir + ".tar.bz2");
      return;
    }

    // look for consensus files
    System.out.printf("%nLooking for relevant consensuses between "
        + "%tF %<tT and %s...%n", calFrom, timestampStr);
    SortedSet<File> tooOldConsensuses = new TreeSet<File>();
    SortedSet<File> relevantConsensuses = new TreeSet<File>();
    SortedSet<File> tooNewConsensuses = new TreeSet<File>();
    directoriesLeftToParse.clear();
    for (File consensusDir : consensusDirs)
      directoriesLeftToParse.push(consensusDir);
    SimpleDateFormat consensusTimeFormat = new SimpleDateFormat(
        "yyyy-MM-dd-HH-mm-ss");
    consensusTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    while (!directoriesLeftToParse.isEmpty()) {
      File directoryOrFile = directoriesLeftToParse.pop();
      if (directoryOrFile.isDirectory()) {
        for (File fileInDir : directoryOrFile.listFiles()) {
          directoriesLeftToParse.push(fileInDir);
        }
        continue;
      } else {
        String filename = directoryOrFile.getName();
        if (filename.endsWith("consensus")) {
          long consensusTime = consensusTimeFormat.parse(
              filename.substring(0, 19)).getTime();
          if (consensusTime >= timestampTooOld &&
              consensusTime < timestampFrom)
            tooOldConsensuses.add(directoryOrFile);
          else if (consensusTime >= timestampFrom &&
                   consensusTime <= timestamp)
            relevantConsensuses.add(directoryOrFile);
          else if (consensusTime > timestamp &&
                   consensusTime <= timestampTooNew)
            tooNewConsensuses.add(directoryOrFile);
        }
      }
    }
    SortedSet<File> allConsensuses = new TreeSet<File>();
    allConsensuses.addAll(tooOldConsensuses);
    allConsensuses.addAll(relevantConsensuses);
    allConsensuses.addAll(tooNewConsensuses);
    if (allConsensuses.isEmpty()) {
      System.out.println("  None found!\n\n" + DELIMITER + "\n\nResult is "
          + "INDECISIVE!\n\nWe cannot make any statement about IP address "
          + relayIP + " being a relay at " + timestampStr + " or not! We "
          + "did not find any relevant consensuses preceding the given "
          + "time. This either means that you did not download and "
          + "extract the consensus archives preceding the hours before "
          + "the given time, or (in rare cases) that the directory "
          + "archives are missing the hours before the timestamp. Please "
          + "check that your directory archives contain consensus files "
          + "of the interval 5:00 hours before and 2:00 hours after the "
          + "time you are looking for.\n");
      return;
    }
    for (File f : relevantConsensuses)
      System.out.println("  " + f.getAbsolutePath());

    // parse consensuses to find descriptors belonging to the IP address
    System.out.println("\nLooking for descriptor identifiers referenced "
        + "in \"r \" lines in these consensuses containing IP address "
        + relayIP + "...");
    SortedSet<File> positiveConsensusesNoTarget = new TreeSet<File>();
    Set<String> addressesInSameNetwork = new HashSet<String>();
    SortedMap<String, Set<File>> relevantDescriptors =
        new TreeMap<String, Set<File>>();
    for (File consensus : allConsensuses) {
      if (relevantConsensuses.contains(consensus))
        System.out.println("  " + consensus.getAbsolutePath());
      BufferedReader br = new BufferedReader(new FileReader(consensus));
      String line;
      while ((line = br.readLine()) != null) {
        if (!line.startsWith("r "))
          continue;
        String[] parts = line.split(" ");
        String address = parts[6];
        if (address.equals(relayIP)) {
          byte[] result = Base64.decode(parts[3] + "==");
          String hex = new BigInteger(1, Base64.decode(parts[3] +
              "==")).toString(16).substring(0, 40);
          if (!relevantDescriptors.containsKey(hex))
            relevantDescriptors.put(hex, new HashSet<File>());
          relevantDescriptors.get(hex).add(consensus);
          positiveConsensusesNoTarget.add(consensus);
          if (relevantConsensuses.contains(consensus))
            System.out.println("    \"" + line + "\" references "
                + "descriptor " + hex);
        } else {
          if (relayIP.startsWith(address.substring(0,
              address.lastIndexOf(".")))) {
            addressesInSameNetwork.add(address);
          }
        }
      }
      br.close();
    }
    if (relevantDescriptors.isEmpty()) {
      System.out.printf("  None found!\n\n" + DELIMITER + "\n\nResult is "
          + "NEGATIVE with moderate certainty!\n\nWe did not find IP "
          + "address " + relayIP + " in any of the consensuses that were "
          + "published between %tF %<tT and %tF %<tT.\n\nA possible "
          + "reason for false negatives is that the relay is using a "
          + "different IP address when generating a descriptor than for "
          + "exiting to the Internet. We hope to provide better checks "
          + "for this case in the future.", calTooOld, calTooNew);
      if (!addressesInSameNetwork.isEmpty()) {
        System.out.println("\n\nThe following other IP addresses of Tor "
            + "relays were found in the mentioned consensus files that "
            + "are in the same /24 network and that could be related to "
            + "IP address " + relayIP + ":");
        for (String s : addressesInSameNetwork) {
          System.out.println("  " + s);
        }
      }
      System.out.println();
      return;
    }

    // parse router descriptors to check exit policies
    SortedSet<File> positiveConsensuses = new TreeSet<File>();
    Set<String> missingDescriptors = new HashSet<String>();
    if (target != null) {
      System.out.println("\nChecking if referenced descriptors permit "
          + "exiting to " + target + "...");
      Set<String> descriptors = relevantDescriptors.keySet();
      missingDescriptors.addAll(relevantDescriptors.keySet());
      directoriesLeftToParse.clear();
      for (File descriptorsDir : descriptorsDirs)
        directoriesLeftToParse.push(descriptorsDir);
      while (!directoriesLeftToParse.isEmpty()) {
        File directoryOrFile = directoriesLeftToParse.pop();
        if (directoryOrFile.isDirectory()) {
          for (File fileInDir : directoryOrFile.listFiles()) {
            directoriesLeftToParse.push(fileInDir);
          }
          continue;
        } else {
          String filename = directoryOrFile.getName();
          for (String descriptor : descriptors) {
            if (filename.equals(descriptor)) {
              missingDescriptors.remove(descriptor);
              BufferedReader br = new BufferedReader(
                  new FileReader(directoryOrFile));
              String line;
              while ((line = br.readLine()) != null) {
                if (line.startsWith("reject ") ||
                    line.startsWith("accept ")) {
                  boolean ruleAccept = line.split(" ")[0].equals("accept");
                  String ruleAddress = line.split(" ")[1].split(":")[0];
                  if (!ruleAddress.equals("*")) {
                    if (!ruleAddress.contains("/") &&
                        !ruleAddress.equals(targetIP))
                      continue; // IP address does not match
                    String[] ruleIPParts = ruleAddress.split("/")[0].
                        replace(".", " ").split(" ");
                    int ruleNetwork = Integer.parseInt(
                        ruleAddress.split("/")[1]);
                    for (int i = 0; i < 4; i++) {
                      if (ruleNetwork == 0) {
                        break;
                      } else if (ruleNetwork >= 8) {
                        if (ruleIPParts[i].equals(targetIPParts[i]))
                          ruleNetwork -= 8;
                        else
                          break;
                      } else {
                        int mask = 255 ^ 255 >>> ruleNetwork;
                        if ((Integer.parseInt(ruleIPParts[i]) & mask) ==
                            (Integer.parseInt(targetIPParts[i]) & mask))
                          ruleNetwork = 0;
                        break;
                      }
                    }
                    if (ruleNetwork > 0)
                      continue; // IP address does not match
                  }
                  String rulePort = line.split(" ")[1].split(":")[1];
                  if (targetPort == null && !ruleAccept &&
                      !rulePort.equals("*"))
                    continue; // with no port given, we only consider
                              // reject :* rules as matching
                  if (targetPort != null && !rulePort.equals("*") &&
                      rulePort.contains("-")) {
                    int fromPort = Integer.parseInt(
                        rulePort.split("-")[0]);
                    int toPort = Integer.parseInt(rulePort.split("-")[1]);
                    int targetPortInt = Integer.parseInt(targetPort);
                    if (targetPortInt < fromPort ||
                        targetPortInt > toPort) {
                      continue; // port not contained in interval
                    }
                  }
                  if (targetPort != null) {
                    if (!rulePort.equals("*") &&
                        !rulePort.contains("-") &&
                        !targetPort.equals(rulePort))
                      continue; // ports do not match
                  }
                  boolean relevantMatch = false;
                  for (File f : relevantDescriptors.get(descriptor))
                    if (relevantConsensuses.contains(f))
                      relevantMatch = true;
                  if (relevantMatch)
                    System.out.println("  "
                        + directoryOrFile.getAbsolutePath() + " "
                        + (ruleAccept ? "permits" : "does not permit")
                        + " exiting to " + target + " according to rule \""
                        + line + "\"");
                  if (ruleAccept)
                    positiveConsensuses.addAll(
                        relevantDescriptors.get(descriptor));
                  break;
                }
              }
              br.close();
            }
          }
        }
      }
    }

    // print out result
    Set<File> matches = (target != null) ? positiveConsensuses
                                         : positiveConsensusesNoTarget;
    if (matches.contains(relevantConsensuses.last())) {
      System.out.println("\n" + DELIMITER + "\n\nResult is POSITIVE with "
          + "high certainty!\n\nWe found one or more relays on IP address "
          + relayIP
          + (target != null ? " permitting exit to " + target : "")
          + " in the most recent consensus preceding " + timestampStr
          + " that clients were likely to know.\n");
      return;
    }
    boolean resultIndecisive = target != null
        && !missingDescriptors.isEmpty();
    if (resultIndecisive) {
      System.out.println("\n" + DELIMITER + "\n\nResult is INDECISIVE!\n\n"
          + "At least one referenced descriptor could not be found. This "
          + "is a rare case, but one that (apparently) happens. We cannot "
          + "make any good statement about exit relays without these "
          + "descriptors. The following descriptors are missing:");
      for (String desc : missingDescriptors)
        System.out.println("  " + desc);
    }
    boolean inOtherRelevantConsensus = false, inTooOldConsensuses = false,
        inTooNewConsensuses = false;
    for (File f : matches)
      if (relevantConsensuses.contains(f))
        inOtherRelevantConsensus = true;
      else if (tooOldConsensuses.contains(f))
        inTooOldConsensuses = true;
      else if (tooNewConsensuses.contains(f))
        inTooNewConsensuses = true;
    if (inOtherRelevantConsensus) {
      if (!resultIndecisive)
        System.out.println("\n" + DELIMITER + "\n\nResult is POSITIVE "
            + "with moderate certainty!");
      System.out.println("\nWe found one or more relays on IP address "
          + relayIP
          + (target != null ? " permitting exit to " + target : "")
          + ", but not in the consensus immediately preceding "
          + timestampStr + ". A possible reason for the relay being "
          + "missing in the last consensus preceding the given time might "
          + "be that some of the directory authorities had difficulties "
          + "connecting to the relay. However, clients might still have "
          + "used the relay.");
    } else {
      if (!resultIndecisive)
        System.out.println("\n" + DELIMITER + "\n\nResult is NEGATIVE "
            + "with high certainty!");
      System.out.println("\nWe did not find any relay on IP address "
          + relayIP
          + (target != null ? " permitting exit to " + target : "")
          + " in the consensuses 3:00 hours preceding " + timestampStr
          + ".");
      if (inTooOldConsensuses || inTooNewConsensuses) {
        if (inTooOldConsensuses && !inTooNewConsensuses)
          System.out.println("\nNote that we found a matching relay in "
              + "consensuses that were published between 5:00 and 3:00 "
              + "hours before " + timestampStr + ".");
        else if (!inTooOldConsensuses && inTooNewConsensuses)
          System.out.println("\nNote that we found a matching relay in "
              + "consensuses that were published up to 2:00 hours after "
              + timestampStr + ".");
        else
          System.out.println("\nNote that we found a matching relay in "
              + "consensuses that were published between 5:00 and 3:00 "
              + "hours before and in consensuses that were published up "
              + "to 2:00 hours after " + timestampStr + ".");
        System.out.println("Make sure that the timestamp you provided is "
            + "in the correct timezone: UTC (or GMT).");
      }
    }
    if (target != null) {
      if (positiveConsensuses.isEmpty() &&
          !positiveConsensusesNoTarget.isEmpty())
        System.out.println("\nNote that although the found relay(s) did "
            + "not permit exiting to " + target + ", there have been one "
            + "or more relays running at the given time.");
    }
    System.out.println();
  }
}

