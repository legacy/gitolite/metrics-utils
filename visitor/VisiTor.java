/* Copyright 2010 The Tor Project
 * See LICENSE for licensing information */

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import java.util.zip.*;

public final class VisiTor {

  public static void main(String[] args) {

    /* Check parameters. */
    if (args.length < 3 || args.length > 4) {
      System.out.println("\nUsage: java "
          + VisiTor.class.getSimpleName()
          + " <web server log> <exit list directory> <output file> "
          + "[<server log part with Tor user requests>]\n");
      return;
    }
    String webServerLog = args[0];
    String exitListDirectory = args[1];
    String outputFile = args[2];
    String serverLogPartTorUsers = args.length == 4 ? args[3] : null;

    /* Initialize regular expressions to detect Torbutton user agents. The
     * user-agent string in Torbutton was changed in the following Git
     * commits: 48b8300, 0776f7e, cc15032, 6fdfd5a */
    SortedMap<String, Pattern> torbuttonUserAgents =
        new TreeMap<String, Pattern>();
    torbuttonUserAgents.put("torbutton1_2_5_tails", Pattern.compile(
        "Mozilla/5\\.0 \\(Windows; U; Windows NT 6\\.1; "
        + "chrome://global/locale/intl.properties; rv\\:1\\.9\\.2\\.3\\) "
        + "Gecko/20100401 Firefox/3\\.6\\.3"));
    torbuttonUserAgents.put("torbutton1_2_5", Pattern.compile(
        "Mozilla/5\\.0 \\(Windows; U; Windows NT 6\\.1; "
        + "[a-z]{2}-[A-Z]{2}; rv\\:1\\.9\\.2\\.3\\) "
        + "Gecko/20100401 Firefox/3\\.6\\.3"));
    torbuttonUserAgents.put("torbutton1_2_1", Pattern.compile(
        "Mozilla/5\\.0 \\(Windows; U; Windows NT 5\\.1; "
        + "en-US; rv\\:1\\.9\\.0\\.7\\) "
        + "Gecko/2009021910 Firefox/3\\.0\\.7"));
    torbuttonUserAgents.put("torbutton1_2_0", Pattern.compile(
        "Mozilla/5\\.0 \\(Windows; U; Windows NT 5\\.1; "
        + "[a-z]{2}-[A-Z]{2}; rv\\:1\\.8\\.1\\.16\\) "
        + "Gecko/20080702 Firefox/2\\.0\\.0\\.16"));
    torbuttonUserAgents.put("torbutton1_2_0rc1", Pattern.compile(
        "Mozilla/5\\.0 \\(Windows; U; Windows NT 5\\.1; "
        + "en-US; rv\\:1\\.8\\.1\\.14\\) "
        + "Gecko/20080404 Firefox/2\\.0\\.0\\.14"));

    /* Read the first line of the web server log to let the user know
     * early if we think we can't parse it. */
    System.out.print("Reading the first line of your web server log "
        + (webServerLog.equals("-") ? "from stdin" : "'" + webServerLog
        + "'") + " to see if we can parse it... ");
    SimpleDateFormat logFormat = new SimpleDateFormat(
        "[dd/MMM/yyyy:HH:mm:ss Z]");
    logFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    Pattern ipAddressPattern = Pattern.compile("(\\d+\\.){3}\\d+");
    BufferedReader webServerLogReader = null;
    String logLine = null;
    if (webServerLog.equals("-")) {
      try {
        webServerLogReader = new BufferedReader(new InputStreamReader(
            System.in));
        logLine = webServerLogReader.readLine();
      } catch (IOException e) {
        System.out.println("FAILED\nCould not read from stdin! Exiting!");
        e.printStackTrace();
        return;
      }
    } else {
      File logFile = new File(webServerLog);
      if (!logFile.exists()) {
        System.out.println("FAILED\nFile does not exist! Exiting!");
        return;
      }
      try {
        if (webServerLog.endsWith(".gz")) {
          webServerLogReader = new BufferedReader(new InputStreamReader(
              new GZIPInputStream(new FileInputStream(webServerLog))));
        } else {
          webServerLogReader = new BufferedReader(new FileReader(
              webServerLog));
        }
        logLine = webServerLogReader.readLine();
      } catch (IOException e) {
        System.out.println("FAILED\nCould not read file! Exiting!");
        e.printStackTrace();
        return;
      }
    }
    if (logLine == null) {
      System.out.println("FAILED\nLog file is empty! Exiting!");
      return;
    }
    if (!ipAddressPattern.matcher(logLine.split(" ")[0]).matches()) {
      System.out.print("WARN\nFirst column may not contain IP address! "
          + "Trying to parse anyway... ");
    }
    try {
      logFormat.parse(logLine.split(" ")[3] + " "
          + logLine.split(" ")[4]);
    } catch (ParseException e) {
      System.out.println("FAILED\nFourth column does not contain "
          + "timestamp! Exiting!");
      e.printStackTrace();
      return;
    }
    System.out.println("OK");

    /* Make a list of all exit list archives we're going to read from
     * disk. */
    System.out.print("Creating file list of exit list archives... ");
    SortedSet<File> exitListFiles = new TreeSet<File>();
    Stack<File> fileStack = new Stack<File>();
    fileStack.push(new File(exitListDirectory));
    SimpleDateFormat exitListFilenameFormat = new SimpleDateFormat(
        "yyyy-MM-dd-HH-mm-ss");
    exitListFilenameFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    long firstExitList = -1L, lastExitList = -1L;
    SimpleDateFormat isoDateTimeFormat = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss");
    isoDateTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    while (!fileStack.empty()) {
      File dirOrFile = fileStack.pop();
      if (dirOrFile.isDirectory()) {
        for (File file : dirOrFile.listFiles()) {
          fileStack.push(file);
        }
      } else {
        try {
          long timestamp = exitListFilenameFormat.parse(
              dirOrFile.getName()).getTime();
          if (firstExitList < 0 || timestamp < firstExitList) {
            firstExitList = timestamp;
          }
          if (lastExitList < 0 || timestamp > lastExitList) {
            lastExitList = timestamp;
          }
          exitListFiles.add(dirOrFile);
        } catch (ParseException e) {
          /* Must be an unrelated file. Ignore it. */
        }
      }
    }
    if (exitListFiles.isEmpty()) {
      System.out.println("FAILED\nWe didn't find a single exit list. Did "
          + "you extract the exit list archives? Exiting.");
      return;
    }
    System.out.println("OK\nWe found " + exitListFiles.size()
        + " exit lists between " + isoDateTimeFormat.format(firstExitList)
        + " and " + isoDateTimeFormat.format(lastExitList) + ".");

    /* Read all ExitAddress lines from the provided exit list archives
     * and store them in a sorted set. */
    System.out.print("Parsing exit list archives... ");
    SortedSet<String> exitAddressLines = new TreeSet<String>();
    try {
      for (File exitListFile : exitListFiles) {
        BufferedReader br = new BufferedReader(new FileReader(
            exitListFile));
        String line = null;
        while ((line = br.readLine()) != null) {
          if (line.startsWith("ExitAddress ")) {
            exitAddressLines.add(line);
          }
        }
        br.close();
      }
    } catch (IOException e) {
      System.out.println("FAILED\nCould not read exit list archive! "
          + "Exiting!");
      e.printStackTrace();
      return;
    }
    System.out.println("OK");

    /* When parsing the log file, try to find an ExitAddress line for the
     * given IP address that is not older than 24 hours. If such a line
     * exists, count this request as coming from a Tor user. Add a dummy
     * entry for IP address 0.0.0.0, so there's always a smaller element
     * than the IP address we're looking up. */
    System.out.print("Parsing web server log file... ");
    Map<String, Integer> torRequests = new HashMap<String, Integer>();
    Map<String, Integer> nonTorRequests = new HashMap<String, Integer>();
    Map<String, Integer> torbuttonRequests =
        new HashMap<String, Integer>();
    exitAddressLines.add("ExitAddress 0.0.0.0 1970-01-01");
    SimpleDateFormat exitAddressFormat = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss");
    exitAddressFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    isoDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    SortedSet<String> allDates = new TreeSet<String>();
    try {
      BufferedWriter bw = serverLogPartTorUsers == null ? null
          : new BufferedWriter(new FileWriter(serverLogPartTorUsers));
      boolean haveWarnedAboutMissingExitListsStart = false,
          haveWarnedAboutMissingExitListsEnd = false;
      do {
        String[] lineParts = logLine.split(" ");
        long timestamp = -1L;
        try {
          timestamp = logFormat.parse(lineParts[3] + " " + lineParts[4]).
              getTime();
        } catch (ParseException e) {
          System.out.println("FAILED\nCould not parse timestamp in web "
              + "server log file! Trying to write results anyway.");
          e.printStackTrace();
          break;
        }
        if (timestamp < firstExitList) {
          if (!haveWarnedAboutMissingExitListsStart) {
            System.out.print("WARN\nWe are missing exit lists before "
                + isoDateTimeFormat.format(firstExitList) + " that we "
                + "need to parse the beginning of the server log file!\n"
                + "Skipping... ");
            haveWarnedAboutMissingExitListsStart = true;
          }
          continue;
        }
        if (timestamp > lastExitList) {
          if (!haveWarnedAboutMissingExitListsEnd) {
            System.out.print("WARN\nWe are missing exit lists after "
                + isoDateTimeFormat.format(lastExitList) + " that we "
                + "need to parse the end of the server log file!\n"
                + "Skipping... ");
            haveWarnedAboutMissingExitListsEnd = true;
          }
          continue;
        }
        String currentDate = isoDateFormat.format(timestamp);
        allDates.add(currentDate);
        String address = lineParts[0];
        String exitAddressLine = "ExitAddress " + address + " "
            + exitAddressFormat.format(timestamp);
        String previousExitAddressLine = exitAddressLines.headSet(
            exitAddressLine).last();
        String[] exitAddressLineParts = previousExitAddressLine.
            split(" ");
        boolean isTorUser = false;
        if (exitAddressLineParts[1].equals(address)) {
          long lastTestedTimestamp = -1L;
          try {
            lastTestedTimestamp = exitAddressFormat.parse(
                exitAddressLineParts[2] + " " + exitAddressLineParts[3]).
                getTime();
          } catch (ParseException e) {
            System.out.println("FAILED\nCould not parse timestamp in "
                + "exit list file! Trying to write results anyway.");
            e.printStackTrace();
            break;
          }
          if ((timestamp - lastTestedTimestamp) <=
              24L * 60L * 60L * 1000L) {
            isTorUser = true;
          }
        }
        if (isTorUser) {
          if (bw != null) {
            bw.write(logLine + "\n");
          }
          int requestsSoFar = torRequests.containsKey(currentDate)
              ? torRequests.get(currentDate) : 0;
          torRequests.put(currentDate, requestsSoFar + 1);
          String userAgentString = logLine.trim().split("\"")[
              logLine.trim().split("\"").length - 1];
          for (Map.Entry<String, Pattern> e :
              torbuttonUserAgents.entrySet()) {
            if (e.getValue().matcher(userAgentString).matches()) {
              String torbuttonRequestKey = currentDate + "," + e.getKey();
              int requests = torbuttonRequests.containsKey(
                  torbuttonRequestKey) ? torbuttonRequests.get(
                  torbuttonRequestKey) : 0;
              torbuttonRequests.put(torbuttonRequestKey, requests + 1);
            }
          }
        } else {
          int requestsSoFar = nonTorRequests.containsKey(currentDate)
              ? nonTorRequests.get(currentDate) : 0;
          nonTorRequests.put(currentDate, requestsSoFar + 1);
        }
      } while ((logLine = webServerLogReader.readLine()) != null);
      webServerLogReader.close();
      if (bw != null) {
        bw.close();
      }
      System.out.println("OK");
    } catch (IOException e) {
      System.out.println("FAILED\nCould not read web server log file"
          + (serverLogPartTorUsers == null ? "" : " or write the server "
          + "log file part with Tor user requests") + "! Trying to write "
          + "results anyway.");
      e.printStackTrace();
    }

    /* Check if there's anything to write. */
    if (torRequests.isEmpty() && nonTorRequests.isEmpty()) {
      System.out.println("Nothing to write. Exiting!");
      return;
    }

    /* Write output to disk. */
    System.out.print("Writing output to disk... ");
    try {
      BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
      bw.write("date,tor,nottor");
      for (String torbuttonUserAgent : torbuttonUserAgents.keySet()) {
        bw.write("," + torbuttonUserAgent);
      }
      bw.write("\n");
      String currentDate = allDates.first(), lastDate = allDates.last();
      while (currentDate.compareTo(lastDate) <= 0) {
        if (!torRequests.containsKey(currentDate) &&
            !nonTorRequests.containsKey(currentDate)) {
          bw.write(currentDate + ",NA,NA");
          for (int i = 0; i < torbuttonUserAgents.size(); i++) {
            bw.write(",NA");
          }
          bw.write("\n");
        } else {
          bw.write(currentDate + ","
              + (torRequests.containsKey(currentDate)
              ? torRequests.get(currentDate) : "0") + ","
              + (nonTorRequests.containsKey(currentDate)
              ? nonTorRequests.get(currentDate) : "0"));
          for (Map.Entry<String, Pattern> e :
              torbuttonUserAgents.entrySet()) {
            String torbuttonRequestKey = currentDate + "," + e.getKey();
            bw.write("," + (torbuttonRequests.containsKey(
                torbuttonRequestKey) ? torbuttonRequests.get(
                torbuttonRequestKey) : "0"));
          }
          bw.write("\n");
        }
        try {
          currentDate = isoDateFormat.format(isoDateFormat.parse(
              currentDate).getTime() + 24L * 60L * 60L * 1000L);
        } catch (ParseException e) {
          System.out.println("FAILED\nCould not write output file! "
              + "Exiting!");
          return;
        }
      }
      bw.close();
    } catch (IOException e) {
      System.out.println("FAILED\nCould not write output file! Exiting!");
      e.printStackTrace();
      return;
    }
    System.out.println("OK");
  }
}

